import React from 'react';
import ReactDOM from 'react-dom';
import Main from './pages/main';

ReactDOM.render(<Main />, document.querySelector('#root'));
