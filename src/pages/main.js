import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import classNames from 'classnames';
import Server from '../components/server';
import withRoot from '../withRoot';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Menu, { MenuItem } from 'material-ui/Menu';

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 1,
    margin: theme.spacing.unit * 1,
    display: 'flex',
    alignItems: 'center',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  flex: {
    flex: 1,
    textAlign: "center"
  },
  container: {
    display: "flex",
  }
});

class Main extends React.Component {
  state = {
    spacing: '16',
    serverLoad: '100',
    anchorEl: null,
    servers: [0]
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  addServer = (event, value) => {
    const newServers = this.state.servers.concat([this.state.servers.length])
    this.setState({
      servers: newServers
    });
  };

  render() {
    const { classes } = this.props;
    const { servers } = this.state;
    const { anchorEl } = this.state;

    return (
      <Paper className={classNames(classes.paper)} >
        <Grid container >
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={this.handleClose.bind(this)}>
            <MenuItem onClick={this.handleClose.bind(this)}>Profile</MenuItem>
            <MenuItem onClick={this.handleClose.bind(this)}>My account</MenuItem>
            <MenuItem onClick={this.handleClose.bind(this)}>Logout</MenuItem>
          </Menu>
          <Grid item xs={12}>
            <AppBar position="static">
              <Toolbar>
                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.handleClick.bind(this)}>
                  <MenuIcon />
                </IconButton>
                <Button color="inherit" onClick={this.addServer.bind(this)}>Add</Button>
                <Typography variant="title" color="inherit" className={classes.flex}>
                  Kuber Loader
               </Typography>
                <Button color="inherit">Login</Button>
              </Toolbar>
            </AppBar>
          </Grid>
          <Grid item xs={12}>
            {
              servers.map((n,i) => {
              return <Server key={i}/>
            })
            }
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(Main));
