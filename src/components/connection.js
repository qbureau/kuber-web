export default class Connection {
    serverAddress
    wsConnection
    onMessage

    constructor(serverAddress, onMessage, ready) {
        this.serverAddress = serverAddress
        this.onMessage = onMessage

        if (window["WebSocket"]) {
            this.wsConnection = new WebSocket("ws://" + serverAddress + "/ws")

            this.wsConnection.onopen = (evt) => {
                console.log(`connected to server ${this.serverAddress}`, evt)
                ready(true)
            }

            this.wsConnection.onclose = (evt) => {
                console.log(` server ${this.serverAddress}, closed connection`, evt)
            }
            this.wsConnection.onerror = (evt) => {
                console.log(` server ${this.serverAddress}, error`, evt)
                ready(false)
            }
            this.wsConnection.onmessage = (evt) => {
                console.log(evt.data)
                this.onMessage(JSON.parse(evt.data))
            }
        } else {
            console.error("No websocket in this browser!")
        }
   }

   sendCommand(data) {
        this.wsConnection.send(data)
   }


}