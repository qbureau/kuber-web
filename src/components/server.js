import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import Grid from 'material-ui/Grid'
import Paper from 'material-ui/Paper'
import Button from 'material-ui/Button'
import classNames from 'classnames'
import TextField from 'material-ui/TextField'
import Clear from 'material-ui-icons/Clear'
import Connection from './connection'
//import { toClass } from 'recompose'

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 1,
    margin: theme.spacing.unit * 1,
    display: 'flex',
    alignItems: 'center',
  },
  button: {
    margin: theme.spacing.unit * 2,
  },
  textFieldServer: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,

  },
  textFieldLoad: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 100,
  },
  textFieldOutput: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 550
  },
  textFieldOutputTextArea: {
    fontSize: 10
  },
  container: {
    display: "flex",
    //    flexWrap: "wrap",
    //    justify: 'center',
  }
})

//const TextFieldWrapper = toClass(TextField)

class Server extends React.Component {
  connection
//  textFieldOutputRef
  responses = {}
  pid
  intervalId
//  pendingStressCommand
  textFieldOutputId

  constructor(props) {
    super(props)
    this.state = {
      realLoad: 'NS',
      serverLoad: '10',
      serverAddress: 'localhost:8080',
      textFieldOutput: ''
    }
    this.scrollToBottom = this.scrollToBottom.bind(this)
    this.textFieldOutputId = "textFieldOutputId"+ Math.random()
  }

  processResponse(command) {
    let body = this.responses[command]
    if (command.startsWith("stress")) {
      //get stress-ng process id, so it'll be killed then command
      this.pid = (/pid:\[(.+)\]/g).exec(body)[1]
      this.responses[command] =""
    }
    else if (command.startsWith("psstress")) {
      //get stress-ng process id, so it'll be killed then
      let response = this.responses[command]
      console.log(`${command} ${response}`)
      let load;
      try {
        let res = (/\n(\S+)\s+.+\s+.+\n/g).exec(body)
        if (res) {
          load = res[1]
        } else {
          load = "unknown"
        }
      }
      catch (e) {
        load = e
      }
      this.setState({realLoad:load})
      this.responses[command] =""
    }
    else if (command.startsWith("kill")) {
      this.pid = null;
      this.responses[command] =""
    }
  }

  responseGluer(response){
    this.responses[response.command] = this.responses[response.command] + response.body
    if (response.done){
      this.processResponse(response.command)
    }
  }

  sendCommand(command){
    this.responses[command]=""
    this.connection.sendCommand(command)
  }

  handleClick = event => {
    if  (this.connection){
      if (this.state.serverLoad === "0") {
        this.sendCommand("kill " + this.pid)
        //clearInterval(this.intervalID);
      } else {
        this.sendCommand("stress " + this.state.serverLoad)
      }

    } else {
      this.connection = new Connection(this.state.serverAddress, data => {
          this.responseGluer(data)
          this.setState((prevState) => {
            return {textFieldOutput: prevState.textFieldOutput + data.body}
          }, () => {
            this.scrollToBottom()
          })
        },
        ready => {
          if (ready) {
            this.sendCommand("stress " + this.state.serverLoad)
            this.intervalID = setInterval(() => {
              this.sendCommand("psstress")
            }, 1000);
          }
        })
    }
  }

  handleChange = (key, e) => {
    this.setState({[key]: e.target.value})
  }
  componentDidMount() {
    this.scrollToBottom()
  }
  scrollToBottom() {
    let elem = document.getElementById(this.textFieldOutputId)
    if (elem) {
      elem.scrollTop = elem.scrollHeight
    }
  }

  render() {
    const {classes} = this.props
    const {realLoad} = this.state

    return (
      <Grid className={classes.container}>
        <Paper className={classNames(classes.paper)}>
          <TextField
            id="serverAddress"
            name="serverAddress"
            label="Server Address"
            className={classes.textFieldServer}
            value={this.state.serverAddress}
            onChange={this.handleChange.bind(this, "serverAddress")}
            margin="normal"
          />
          <TextField
            id="serverLoad"
            name="serverLoad"
            label="Server Load"
            className={classes.textFieldLoad}
            value={this.state.serverLoad}
            onChange={this.handleChange.bind(this, "serverLoad")}
            margin="normal"
          />
          <Button variant="raised" className={classes.button}
                  onClick={this.handleClick.bind(this)}>set</Button>
        </Paper>
        <Paper className={classes.paper}>
          <p className={{}}>Load: {realLoad}</p>
        </Paper>
        <Paper className={classes.paper}>
          <TextField
            multiline
            rows="3"
            id={this.textFieldOutputId}
            label="Log"
            name="textFieldOutput"
            className={classes.textFieldOutput}
            value={this.state.textFieldOutput}
            onChange={this.handleChange.bind(this, 'textFieldOutput')}
            margin="normal"
//            textareaStyle={{fontSize: "10"}}
//            ref={(input) => { this.textFieldOutputRef = input }}
          />

        </Paper>
        <div className={classes.paper}>
          <Clear fontSize style={{fontSize: 50}}/>
        </div>
      </Grid>
    )
  }
}

Server.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Server)
